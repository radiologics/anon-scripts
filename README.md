# README #

This Repo makes DicomEdit anonymization scripts available.

### To download a script...

GET the URL like https://bitbucket.org/radiologics/anon-scripts/raw/<branch>/<path-to-file>

where <branch> is the repo branch, e.g. 'master'
<path-to-file> is the directory path to file in the repo, e.g. test/mr/mr.das

